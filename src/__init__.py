from .app import *
from .schemas import *

__version__ = VERSION


__all__ = [
    "create_app",
    "VERSION",
]
