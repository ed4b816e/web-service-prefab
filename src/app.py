import logging
import os
from pathlib import Path

from fastapi import FastAPI
from fastapi import Request
from fastapi import Response
from fastapi import status
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

from src.utils import logger
from src.views import view

VERSION = "0.1.0"


def create_app() -> FastAPI:
    logger.CustomizeLogger.make_logger(Path(os.environ["LOGGING_CONFIG"]))

    app = FastAPI(title=os.environ["APP_NAME"], debug=False, version=VERSION)
    app.logger = logger

    app.add_middleware(
        CORSMiddleware,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    @app.exception_handler(Exception)
    async def unicorn_exception_handler(request: Request, exc: Exception):
        logging.error(exc)
        return JSONResponse(
            status_code=400,
            content={"message": str(exc)},
        )

    @app.exception_handler(RequestValidationError)
    async def validation_exception_handler(request: Request, exc: RequestValidationError):
        logging.error(exc)
        return JSONResponse(
            status_code=422,
            content={"message": str(exc)},
        )

    @app.get(
        "/",
        response_class=JSONResponse,
        status_code=200,
        description="Запрос проверки состояния сервиса.",
    )
    async def healthcheck_endpoint(response: Response):
        try:
            pass
        except Exception as error:
            response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
            return {"success": False}
        else:
            return {"success": True}

    app.include_router(
        view.router,
        prefix="/api/v1",
        tags=["view"],
    )
    return app
